#!/usr/bin/env python
# license removed for brevity
import rospy, sys
from std_msgs.msg import Float64
import math
import numpy as np
from trac_ik_python.trac_ik import IK

def talker():
	position = Float64() #position vai ser um objeto Float64
	joint = [0,0,0,0,0,0] #posicao atual da junta
	buff_joint = [0,0,0,0,0,0] # posicao anterior da junta
	pub = [] # publica valores na junta
	count = 0
	init = Float64()
	init.data = 0.0
	delta_j2 = 0
	cord = [-0.37730,0.3103,0.3758]
	buff_cord = [-0.37730,0.3103,0.3758]
	values = 0
	ik_solver = IK("origin_link",
               "link7")

	lower_bound, upper_bound = ik_solver.get_joint_limits()

	ik_solver.set_joint_limits([0.0]* ik_solver.number_of_joints, upper_bound)

	seed_state = [0.0] * ik_solver.number_of_joints
	
	# Faz um publisher em todas as juntas e guarda dentro do pub ---------------------------------------------------
	for i in range(1,7) :
		#/uarm3/joint1_position_controller/command
		pub.append(rospy.Publisher('/uarm3/joint'+str(i)+'_position_controller/command', Float64, queue_size=10))
	rospy.init_node('uarm3_talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	#---------------------------------------------------------------------------------------------------------------
	while not rospy.is_shutdown():

		buff_values = values

		for i in range (0,6): 
			buff_joint[i] = joint[i]
		for i in range (0,3):
			buff_cord[i] = cord[i]


		#Pega os valores do usuario do motor --------
		position.data = raw_input('motores: ')
		vet = position.data.split(',')
		for i in range (0,6):
			joint[i] = float(vet[i])

		for a in range (0,6):
			pub[a].publish(math.radians(joint[a]))

		#values = ik_solver.get_ik(seed_state,,,cord[2],0.0,0.0,0.0,0.1)
		#val = str(values)
		#auxVal = val.strip('()')
		#vet = auxVal.split(',')

		#print "values:" + str(values)
		
		'''
		for i in range (0,6):
			joint[i] = float(vet[i])

		if values == None:
			values = buff_values
			print "None"
		
		vel = 0.002

		for a in range (0,3):
			if a == 0:
				if cord[a]>buff_cord[a]:
					for count in np.arange(buff_cord[a],cord[a]+0.001,0.001):
						values = ik_solver.get_ik(seed_state,count,cord[1],cord[2],0.0,0.0,0.0,1.0)
						val = str(values)
						auxVal = val.strip('()')
						vet = auxVal.split(',')
						for i in range (0,4):
							pub[i].publish(float(vet[i]))
					cord[a]=count
				else:
					for count in np.arange(buff_cord[a],cord[a]-0.001,-0.001):
						values = ik_solver.get_ik(seed_state,count,cord[1],cord[2],0.0,0.0,0.0,1.0)
						val = str(values)
						auxVal = val.strip('()')
						vet = auxVal.split(',')
						for i in range (0,4):
							pub[i].publish(float(vet[i]))
					cord[a]=count
			if a == 1:
				if cord[a]>buff_cord[a]:
					for count in np.arange(buff_cord[a],cord[a]+0.000000001,0.0000000001):
						values = ik_solver.get_ik(seed_state,cord[0],count,cord[2],0.0,0.0,0.0,1.0)
						val = str(values)
						auxVal = val.strip('()')
						vet = auxVal.split(',')
						print "count" + str(count)
						for i in range (0,4):
							pub[i].publish(float(vet[i]))
					cord[a]=count
				else:
					for count in np.arange(buff_cord[a],cord[a]-0.0000000001,-0.0000000001):
						values = ik_solver.get_ik(seed_state,cord[0],count,cord[2],0.0,0.0,0.0,1.0)
						val = str(values)
						auxVal = val.strip('()')
						vet = auxVal.split(',')
						print "count" + str(count)
						for i in range (0,4):
							pub[i].publish(float(vet[i]))
					cord[a]=count
			if a == 2:
				if cord[a]>buff_cord[a]:
					for count in np.arange(buff_cord[a],cord[a]+0.001,0.001):
						values = ik_solver.get_ik(seed_state,cord[0],cord[1],count,0.0,0.0,0.0,1.0)
						val = str(values)
						auxVal = val.strip('()')
						vet = auxVal.split(',')
						for i in range (0,4):
							pub[i].publish(float(vet[i]))
					cord[a]=count
				else:
					for count in np.arange(buff_cord[a],cord[a]-0.001,-0.001):
						values = ik_solver.get_ik(seed_state,cord[0],cord[1],count,0.0,0.0,0.0,1.0)
						val = str(values)
						auxVal = val.strip('()')
						vet = auxVal.split(',')
						for i in range (0,4):
							pub[i].publish(float(vet[i]))
					cord[a]=count

		#Pega os valores do usuario do motor --------
		auxCord = raw_input('cordenadas: ')
		aux = auxCord.split(',')

		for i in range(0,7):
			cord[i] = cord[i] + (float(aux[i])/1000)

		print "cordenadas: " + str(cord)
			
		'''
		rate.sleep()

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass