#!/usr/bin/env python
# license removed for brevity
import rospy, sys
from std_msgs.msg import Float64
from control_msgs.msg import JointControllerState

import math
import numpy as np


delta = 0


def callback(data):
	global delta
	delta = data.command

def talker():
	global delta
	position = Float64() #position vai ser um objeto Float64
	joint = [0,0,0,0] #posicao atual da junta
	buff_joint = [0,0,0,0] # posicao anterior da junta
	pub = [] # publica valores na junta
	sub = []
	count = 0
	init = Float64()
	init.data = 0.0
	delta_j2 = 0
	buff_aux = 0
	buff_aux2 = 0
	# Faz um publisher em todas as juntas e guarda dentro do pub ---------------------------------------------------
	for i in range(1,5) :
		pub.append(rospy.Publisher('/magician/motor1'+str(i)+'_position_controller/command', Float64, queue_size=10))
	rospy.init_node('magician_talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	#---------------------------------------------------------------------------------------------------------------

	#for a in range(1,5) :
	#rospy.Subscriber('/magician/joint1_position_controller/state', JointControllerState, callback)

	while not rospy.is_shutdown():

		#print "error {}".format(math.degrees(delta))
		
		# Salva valor anterior da junta ----
		for i in range (0,4): 
			buff_joint[i] = joint[i]

		#Pega os valores do usuario do motor --------
		position.data = raw_input('motores: ')
		vet = position.data.split(',')
		for i in range (0,4):
			joint[i] = float(vet[i])
			
			if (i == 2 and init.data == 0.0):
				print "entrou"
				joint[i+1] = float(vet[i])
				pub[i-2].publish(init.data)
				pub[i-1].publish(init.data)
				pub[i].publish(init.data)
				pub[i+1].publish(init.data)
				init.data = 1
			
		#--------------------------------------------
		vel = 0.02
		
		for i in range (0,4):
			
			# Movimenta o motor 1 sendo a Base -------------------------------
	
			if(buff_joint[i]<joint[i]):
				for count in np.arange(buff_joint[i],joint[i]+vel,vel):
					pub[i].publish(math.radians(float(count)))
			else:
				for count in np.arange(buff_joint[i],joint[i]-vel,-vel):
					pub[i].publish(math.radians(float(count)))
			#-----------------------------------------------------------------

			print "buff_joint: "+ str(buff_joint)
			print "joint     : "+ str(joint)
		rate.sleep()

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass
