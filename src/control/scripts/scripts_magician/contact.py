#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from sensor_msgs.msg import LaserScan
import math, time
import numpy as np
import subprocess
from std_srvs.srv import Empty

A = Float32()
B = Float32()
C = Float32()
D = Float32()
E = Float32()
F = Float32()

def callback(data):
    A.data = data.ranges[0]
    B.data = data.ranges[1]
    C.data = data.ranges[2]

def callback2(data):
    D.data = data.ranges[0]
    E.data = data.ranges[1]
    F.data = data.ranges[2]

def move():
    rospy.Subscriber('/magician1/contact', LaserScan, callback)
    rospy.Subscriber('/magician1/contact2', LaserScan, callback2)
    rospy.init_node('contact', anonymous=False)
    rate = rospy.Rate(0.01)
    while not rospy.is_shutdown():
        if A.data < 0.04 or B.data < 0.04 or C.data < 0.04 or D.data < 0.04 or E.data < 0.04 or F < 0.04:
            rospy.wait_for_service('/magician1/on')
            try:
                # Create a handle for the calling the srv
                turn_on = rospy.ServiceProxy('/magician1/on', Empty)
                time.sleep(0.1)
                # Use this handle just like a normal function and call it
                resp = turn_on()
                print("entrou no gripper")
                
            except rospy.ServiceException, e:
                print "testando" 
            print "on"
        
        else:
            rospy.wait_for_service('/magician1/off')
            try:
                turn_off = rospy.ServiceProxy('/magician1/off', Empty)
                resp = turn_off()
                print("entrou no gripper off")
                
            except rospy.ServiceException, e:
                print "testando off"
            print "off"

if __name__ == '__main__':
    try:
        move()
    except rospy.ROSInterruptException:
        pass