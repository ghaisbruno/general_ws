#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from std_msgs.msg import Bool
from std_srvs.srv import Empty
import math
import numpy as np

def gripper_status(msg):
	
	if msg.data == True:
		print('gripper true')
		#gripper_on()
	else:
		print(msg.data)
		#gripper_off()
		
		
def gripper_on():
	rospy.wait_for_service('/magician1/on')
	try:
        # Create a handle for the calling the srv
		turn_on = rospy.ServiceProxy('/magician1/on', Empty)
        # Use this handle just like a normal function and call it
		resp = turn_on()
		print("entrou no gripper")
		return resp
	except rospy.ServiceException, e:
		print "testando" 

def gripper_off():
	rospy.wait_for_service('/magician1/off')
	try:
		turn_off = rospy.ServiceProxy('/magician1/off', Empty)
		resp = turn_off()
		return resp
	except rospy.ServiceException, e:
		print "off: %s" % e
		
			

		
rospy.init_node("vacuum_gripper_grasping", anonymous=False)
gripper_status_sub = rospy.Subscriber('/magician1/vacuum_gripper/grasping', Bool, gripper_status, queue_size=1)

rospy.spin()
