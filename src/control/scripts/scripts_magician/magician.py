#!/usr/bin/env python
# license removed for brevity
import rospy, sys
from std_msgs.msg import Float64
import math
import numpy as np

def talker():
	position = Float64() #position vai ser um objeto Float64
	joint = [0,0,0,0] #posicao atual da junta
	buff_joint = [0,0,0,0] # posicao anterior da junta
	pub = [] # publica valores na junta
	count = 0
	init = Float64()
	init.data = 0.0
	delta_j2 = 0
	buff_aux = 0
	buff_aux2 = 0
	# Faz um publisher em todas as juntas e guarda dentro do pub ---------------------------------------------------
	for i in range(1,5) :
		pub.append(rospy.Publisher('/magician/joint'+str(i)+'_position_controller/command', Float64, queue_size=10))
	rospy.init_node('magician_talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	#---------------------------------------------------------------------------------------------------------------
	while not rospy.is_shutdown():
		
		# Salva valor anterior da junta ----
		for i in range (0,4): 
			buff_joint[i] = joint[i]

		#Pega os valores do usuario do motor --------
		position.data = raw_input('motores: ')
		vet = position.data.split(',')
		for i in range (0,3):
			joint[i] = float(vet[i])
			
			if (i == 2 and init.data == 0.0):
				print "entrou"
				joint[i+1] = float(vet[i])
				pub[i-2].publish(init.data)
				pub[i-1].publish(init.data)
				pub[i].publish(init.data)
				pub[i+1].publish(init.data)
				init.data = 1
			
		#--------------------------------------------
		vel = 0.2
		
		for i in range (0,4):
			
			# Movimenta o motor 1 sendo a Base -------------------------------
			if i== 0 :
				if(buff_joint[i]<joint[i]):
					for count in np.arange(buff_joint[i],joint[i]+vel,vel):
						pub[0].publish(math.radians(float(count)))
				else:
					for count in np.arange(buff_joint[i],joint[i]-vel,-vel):
						pub[0].publish(math.radians(float(count)))
			#-----------------------------------------------------------------
			
			# Movimenta o motor 2 sendo a shoulder -------------------------------
			elif i == 1:

				delta_j2 = abs(joint[i] - buff_joint[i])
				
				if joint[i] > buff_joint[i]:

					for count in np.arange(0,delta_j2+vel,vel):

						buff_j2 = buff_joint[i] + count
						pub[i].publish(math.radians(buff_j2))

						buff_aux = buff_aux2 - count
						pub[i+1].publish(math.radians(buff_aux ))
					print "buff_aux -: "+ str(buff_aux)
					buff_aux2 = buff_aux

				else:
					for count in np.arange(0,delta_j2+vel,vel):
						
						buff_j2 = buff_joint[i] - count
						pub[i].publish(math.radians(buff_j2))

						buff_aux  = buff_aux2 + count

						pub[i+1].publish(math.radians(buff_aux))
					print "buff_aux -: "+ str(buff_aux)
					buff_aux2 = buff_aux

			# Movimenta o motor 3 sendo o Elbow -------------------------------				
			elif i == 2:
				delta_j3 = abs(joint[i]- buff_joint[i])
				
				if joint[i] > buff_joint[i]:

					for count in np.arange(0,delta_j3+vel,vel):

						buff_j3 = buff_aux2  + count
						pub[i].publish(math.radians(buff_j3))

						
						joint[i+1] = buff_joint[i+1] - count
						pub[i+1].publish(math.radians(joint[i+1]))
							

					buff_joint[i+1] = joint[i+1]
					buff_aux2 = buff_j3

				else:

					for count in np.arange(0,delta_j3+vel,vel):
						
						buff_j3 = buff_aux2  - count
						pub[i].publish(math.radians(buff_j3))
					
						joint[i+1] = buff_joint[i+1] + count
						pub[i+1].publish(math.radians(joint[i+1]))
							
					
					buff_joint[i+1] = joint[i+1]
					buff_aux2 = buff_j3

				print "buff_joint: "+ str(buff_joint)
				print "joint     : "+ str(joint)
		rate.sleep()

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass
