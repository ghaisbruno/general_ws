#!/usr/bin/env python
import rospy
import roslib, math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32
import sys, select, termios, tty

#centro
B6 = Float32()
C6 = Float32()
D6 = Float32()

#direita
B7 = Float32()
C7 = Float32()
D7 = Float32()

#esquerda
B8 = Float32()
C8 = Float32()
D8 = Float32()

linha = 0
normal_vel = 0.07
r = 2
vel = 0.12
ang_vel = vel/r

def s6_callback(data):
    B6.data = data.ranges[1]
    C6.data = data.ranges[4]
    D6.data = data.ranges[7]

def s7_callback(data):
    B7.data = data.ranges[1]
    C7.data = data.ranges[4]
    D7.data = data.ranges[7]

def s8_callback(data):
    B8.data = data.ranges[1]
    C8.data = data.ranges[4]
    D8.data = data.ranges[7]


def move():
    pub = rospy.Publisher('/starter1/cmd_vel', Twist, queue_size=1)
    sub6 = rospy.Subscriber('/starter1/line_follower/sensor2', LaserScan, s6_callback)
    sub7 = rospy.Subscriber('/starter1/line_follower/sensor3', LaserScan, s7_callback)
    sub8 = rospy.Subscriber('/starter1/line_follower/sensor4', LaserScan, s8_callback)

    rospy.init_node('line_follower', anonymous=False)
    rate = rospy.Rate(10)
    twist = Twist()

    while not rospy.is_shutdown():
        if (C6.data<0.028):
            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
            pub.publish(twist)
            if(D7.data<0.028):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                pub.publish(twist)
            elif(B8.data<0.028):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                pub.publish(twist)
        
        else:
            print('nao tem linha')
            linha = 0
        
if __name__ == '__main__':
	try:
		move()
	except rospy.ROSInterruptException:
		pass

