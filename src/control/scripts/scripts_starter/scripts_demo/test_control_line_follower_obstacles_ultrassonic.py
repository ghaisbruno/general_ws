#!/usr/bin/env python
import rospy
import roslib, math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32
import sys, select, termios, tty

#centro
A3 = Float32() 
B3 = Float32()
C3 = Float32()
D3 = Float32()
E3 = Float32()

#direita
A4 = Float32() 
B4 = Float32()
C4 = Float32()
D4 = Float32()
E4 = Float32()

#esquerda
A5 = Float32() 
B5 = Float32()
C5 = Float32()
D5 = Float32()
E5 = Float32()



#centro line follower
B6 = Float32()
C6 = Float32()
D6 = Float32()

#direita line follower
B7 = Float32()
C7 = Float32()
D7 = Float32()

#esquerda line follower
B8 = Float32()
C8 = Float32()
D8 = Float32()

linha = 0
normal_vel = 0.05
r = 2
vel = 0.08
ang_vel = vel/r

def callback(data):
	A3.data= data.ranges[0] 
	B3.data= data.ranges[4]
	C3.data= data.ranges[7]
	D3.data= data.ranges[10]
	E3.data= data.ranges[14]

def s4_callback(data):
	A4.data= data.ranges[0] 
	B4.data= data.ranges[4]
	C4.data= data.ranges[7]
	D4.data= data.ranges[10]
	E4.data= data.ranges[14]

def s5_callback(data):
	A5.data= data.ranges[0] 
	B5.data= data.ranges[4]
	C5.data= data.ranges[7]
	D5.data= data.ranges[10]
	E5.data= data.ranges[14]

def s6_callback(data):
    B6.data = data.ranges[0]
    C6.data = data.ranges[2]
    D6.data = data.ranges[4]

def s7_callback(data):
    B7.data = data.ranges[0]
    C7.data = data.ranges[2]
    D7.data = data.ranges[4]

def s8_callback(data):
    B8.data = data.ranges[0]
    C8.data = data.ranges[2]
    D8.data = data.ranges[4]

def move():
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
    sub3 = rospy.Subscriber('/starter1/laser3/scan', LaserScan, callback)
    sub4 = rospy.Subscriber('/starter1/laser4/scan', LaserScan, s4_callback)
    sub5 = rospy.Subscriber('/starter1/laser5/scan', LaserScan, s5_callback)
    sub6 = rospy.Subscriber('line_follower6', LaserScan, s6_callback)
    sub7 = rospy.Subscriber('line_follower7/right', LaserScan, s7_callback)
    sub8 = rospy.Subscriber('line_follower8/left', LaserScan, s8_callback)

    rospy.init_node('line_follower', anonymous=False)
    rate = rospy.Rate(10)
    twist = Twist()
    a = 1

    while not rospy.is_shutdown():
        if (C3.data>1 and D3.data>1 and B3.data>1):
            
            if (C6.data<0.028):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                pub.publish(twist)
                if(B6.data<0.028 and D6.data>0.028 and a == 1):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                    pub.publish(twist)
                elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                    pub.publish(twist)
                elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                    pub.publish(twist)
                    if (C6.data<0.028 and C8.data<0.028):
                        a = 0
                    else:
                        a = 1
                    if(D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                        pub.publish(twist)
                        a = 1
                elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                    pub.publish(twist)
                    if (C6.data<0.028 and C7.data<0.028):
                        a = 0
                    else:
                        a = 1
                    if(B8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                        a = 1
            else:
                print('nao tem linha')
                linha = 0


        if (C3.data<1 or E5.data<1 or A4.data<1):
            twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang
            pub.publish(twist)

            if (E3.data<1.0):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang
                pub.publish(twist)

            elif (A3.data<1.0):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang
                pub.publish(twist)

            #sensor da direita
            elif (E4.data<1.0):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang
                pub.publish(twist)

            elif (C4.data<1.0):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                pub.publish(twist)
                
                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0


            elif (B4.data<0.8):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang
                pub.publish(twist)
                
                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0



            elif (A4.data<1):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang
                pub.publish(twist)


                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0


            #sensor da esquerda
            elif (A5.data<1.0):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang
                pub.publish(twist)

            elif (C5.data<1.0):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                pub.publish(twist)

                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0


            elif (D5.data<0.8):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang
                pub.publish(twist)
                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0
                
            elif (E5.data<1):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang
                pub.publish(twist)
                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0

        else :
            if (E3.data<1 and E3.data<A3.data):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang
                pub.publish(twist)
                if (C6.data<0.028):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
                    pub.publish(twist)
                    if(B6.data<0.028 and D6.data>0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                        pub.publish(twist)
                    elif(B6.data>0.028 and D6.data<0.028 and a == 1):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                        pub.publish(twist)
                    elif(C8.data<0.028 or B8.data<0.028 or D8.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C8.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(D7.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                            pub.publish(twist)
                            a = 1
                    elif(C7.data<0.028 or B7.data<0.028 or D7.data<0.028):
                        twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                        pub.publish(twist)
                        if (C6.data<0.028 and C7.data<0.028):
                            a = 0
                        else:
                            a = 1
                        if(B8.data<0.028):
                            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                            pub.publish(twist)
                            a = 1
                else:
                    print('nao tem linha')
                    linha = 0

            elif (A3.data<1 and A3.data<E3.data):
                twist.linear.x = vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang
                pub.publish(twist)
        
if __name__ == '__main__':
	try:
		move()
	except rospy.ROSInterruptException:
		pass
