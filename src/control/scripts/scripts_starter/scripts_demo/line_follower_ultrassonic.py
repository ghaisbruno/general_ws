#!/usr/bin/env python
import rospy
import roslib, math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32
import sys, select, termios, tty

#centro
B6 = Float32()
C6 = Float32()
D6 = Float32()

#direita
B7 = Float32()
C7 = Float32()
D7 = Float32()

#esquerda
B8 = Float32()
C8 = Float32()
D8 = Float32()

linha = 0
normal_vel = 0.04
r = 2
vel = 0.08
ang_vel = vel/r


def s6_callback(data):
    B6.data = data.ranges[0]
    C6.data = data.ranges[2]
    D6.data = data.ranges[4]

def s7_callback(data):
    B7.data = data.ranges[0]
    C7.data = data.ranges[2]
    D7.data = data.ranges[4]

def s8_callback(data):
    B8.data = data.ranges[0]
    C8.data = data.ranges[2]
    D8.data = data.ranges[4]


def move():
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
    # sub1 = rospy.Subscriber('line_follower1/sensor1', LaserScan, s1_callback)
    sub1 = rospy.Subscriber('/starter1/line_follower/sensor2', LaserScan, s6_callback)
    sub7 = rospy.Subscriber('/starter1/line_follower/sensor3', LaserScan, s7_callback)
    sub8 = rospy.Subscriber('/starter1/line_follower/sensor4', LaserScan, s8_callback)

    rospy.init_node('line_follower', anonymous=False)
    rate = rospy.Rate(10)
    twist = Twist()
    a = 1

    while not rospy.is_shutdown():
        if (C6.data<0.0173):
            twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
            pub.publish(twist)
            if(B6.data<0.0173 and D6.data>0.0173 and a == 1):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - ang_vel
                pub.publish(twist)
            elif(B6.data>0.0173 and D6.data<0.0173 and a == 1):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                pub.publish(twist)
            elif(C8.data<0.0173 or B8.data<0.0173 or D8.data<0.0173):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 4*ang_vel
                pub.publish(twist)
                if (C6.data<0.0173 and C8.data<0.0173):
                    a = 0
                else:
                    a = 1
                if(D7.data<0.0173):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -ang_vel
                    pub.publish(twist)
                    a = 1
            elif(C7.data<0.0173 or B7.data<0.0173 or D7.data<0.0173):
                twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - 4*ang_vel
                pub.publish(twist)
                if (C6.data<0.0173 and C7.data<0.0173):
                    a = 0
                else:
                    a = 1
                if(B8.data<0.0173):
                    twist.linear.x = normal_vel; twist.linear.y = 0; twist.linear.z = 0
                    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = ang_vel
                    pub.publish(twist)
                    a = 1
        else:
            print('nao tem linha')
            linha = 0
        
if __name__ == '__main__':
	try:
		move()
	except rospy.ROSInterruptException:
		pass
