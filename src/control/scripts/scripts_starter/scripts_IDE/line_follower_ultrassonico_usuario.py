#!/usr/bin/env python
import rospy
import roslib, math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32
import sys, select, termios, tty


C6 = Float32()

linha = 0

def s6_callback(data):
    C6.data = data.ranges[4]
    move()

def move():
    if (C6.data<0.028):
        print('linha detectada')
        linha = 1
        if(C6.data>0.028):
            print('nao tem linha')
            linha = 0
    else:
        print('nao tem linha')
        linha = 0
class Follower:
	def __init__(self):
		self.cmd_vel_pub = rospy.Publisher('cmd_vel',
										Twist, queue_size=1)
		self.twist = Twist()
		self.sub6 = rospy.Subscriber('line_follower6', LaserScan, s6_callback)
        
        
	

rospy.init_node('follower')
follower = Follower()
rospy.spin()
		
