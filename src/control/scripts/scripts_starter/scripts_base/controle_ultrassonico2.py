#!/usr/bin/env python

import rospy, roslib
import math

from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32
'''from sensor_msgs.msg import Float32'''

import sys, select, termios, tty
range_center3 = Float32() #center
range_left3 = Float32()
range_right3 = Float32()
range_left_last3 = Float32()
range_right_last3 = Float32()

range_center4 = Float32() #direita
range_left4 = Float32()
range_right4 = Float32()
range_left_last4 = Float32()
range_right_last4 = Float32()

range_center5 = Float32() #esquerda
range_left5 = Float32()
range_right5 = Float32()
range_left_last5 = Float32()
range_right_last5 = Float32()


normal_vel =0.07
normal_angel_turn =0.69321
last_ob_vel =0.03
last_ob_angel =0.002738
half_angel =0.39321

def callback(data):
	rospy.loginfo(" ranges left %f", data.ranges[11])
	rospy.loginfo(" ranges rigth %f", data.ranges[5])
	range_center3.data= data.ranges[8] 
	range_left3.data= data.ranges[5]
	range_right3.data= data.ranges[11]
	range_left_last3.data= data.ranges[2]
	range_right_last3.data= data.ranges[14]

def s4_callback(data):
	rospy.loginfo(" direita ranges left %f", data.ranges[11])
	rospy.loginfo(" dirita ranges rigth %f", data.ranges[5])
	range_center4.data= data.ranges[8] 
	range_left4.data= data.ranges[5]
	range_right4.data= data.ranges[11]
	range_left_last4.data= data.ranges[2]
	range_right_last4.data= data.ranges[14]


def s5_callback(data):
	rospy.loginfo(" esquerda ranges left %f", data.ranges[11])
	rospy.loginfo(" esquerda ranges rigth %f", data.ranges[5])
	range_center5.data= data.ranges[8] 
	range_left5.data= data.ranges[5]
	range_right5.data= data.ranges[11]
	range_left_last5.data= data.ranges[2]
	range_right_last5.data= data.ranges[14]



def move():
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
	sub3 = rospy.Subscriber('laser3/scan', LaserScan, callback)
	sub4 = rospy.Subscriber('laser4/scan', LaserScan, s4_callback)
	sub5 = rospy.Subscriber('laser5/scan', LaserScan, s5_callback)
	
	rospy.init_node('test', anonymous=False)
	rate = rospy.Rate(10) # 10hz
	twist = Twist()
	while not rospy.is_shutdown():
	


		if (range_center3.data>1):
			twist.linear.x =normal_vel; twist.linear.y = 0; twist.linear.z = 0
			twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
			pub.publish(twist)
			if (range_left_last3.data<1.0):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = last_ob_angel
				pub.publish(twist)

			elif (range_right_last3.data<1.0):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -last_ob_angel
				pub.publish(twist)
				#print(range_center.data)

#Sensor da direita

			elif (range_left_last4.data<1.0):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = last_ob_angel
				pub.publish(twist)

			elif (range_right_last4.data<1.0):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = last_ob_angel
				pub.publish(twist)

#Sensor da esquerda
			elif (range_left_last5.data<1.0):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -last_ob_angel
				pub.publish(twist)

			elif (range_right_last5.data<1.0):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = - last_ob_angel
				pub.publish(twist)




		else :
			if (range_left_last3.data>1) or (range_left3.data>1):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -normal_angel_turn
				pub.publish(twist)


			elif (range_right_last3.data>1) or (range_right3.data>1) :
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = normal_angel_turn
				pub.publish(twist)

			elif (range_left3.data>range_right3.data):
				twist.linear.x = last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = -normal_angel_turn
				pub.publish(twist)
			    
			else :
				twist.linear.x =last_ob_vel; twist.linear.y = 0; twist.linear.z = 0
				twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = normal_angel_turn
				pub.publish(twist)


                   
		rate.sleep()

if __name__ == '__main__':
	try:
		move()
	except rospy.ROSInterruptException:
		pass


