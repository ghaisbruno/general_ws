#!/usr/bin/env python
import rospy, cv2, cv_bridge, numpy
import roslib, math
from sensor_msgs.msg import Image, CameraInfo
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32
import sys, select, termios, tty

A3 = Float32() 
B3 = Float32()
C3 = Float32()
D3 = Float32()
E3 = Float32()

#direita
A4 = Float32() 
B4 = Float32()
C4 = Float32()
D4 = Float32()
E4 = Float32()

#esquerda
A5 = Float32() 
B5 = Float32()
C5 = Float32()
D5 = Float32()
E5 = Float32()

trava = 0
r = 2
vel = 0.07
ang = vel/r

def callback(data):
	A3.data= data.ranges[0] 
	B3.data= data.ranges[4]
	C3.data= data.ranges[7]
	D3.data= data.ranges[10]
	E3.data= data.ranges[14]

def s4_callback(data):
	A4.data= data.ranges[0] 
	B4.data= data.ranges[4]
	C4.data= data.ranges[7]
	D4.data= data.ranges[10]
	E4.data= data.ranges[14]

def s5_callback(data):
	A5.data= data.ranges[0] 
	B5.data= data.ranges[4]
	C5.data= data.ranges[7]
	D5.data= data.ranges[10]
	E5.data= data.ranges[14]

class Follower:
	def __init__(self):
		self.bridge = cv_bridge.CvBridge()
		#cv2.namedWindow("window", 1)
		self.image_sub = rospy.Subscriber('/starter2/camera1/image_raw', 
										Image, self.image_callback)
		self.cmd_vel_pub = rospy.Publisher('cmd_vel',
										Twist, queue_size=1)
		self.twist = Twist()
		self.sub3 = rospy.Subscriber('laser3/scan', LaserScan, callback)
		self.sub4 = rospy.Subscriber('laser4/scan', LaserScan, s4_callback)
		self.sub5 = rospy.Subscriber('laser5/scan', LaserScan, s5_callback)
		print("antes")

	def image_callback(self, msg):
			if (C3.data>1 and D3.data>1 and B3.data>1):
				image = self.bridge.imgmsg_to_cv2(msg,desired_encoding='bgr8')
				hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
				lower_yellow = numpy.array([ 10,  10,  10])
				upper_yellow = numpy.array([255, 255, 250])

				mask = cv2.inRange(hsv, lower_yellow, upper_yellow)

				h, w, d = image.shape
				search_top = 3*h/4
				search_bot = 3*h/4 + 20
				mask[0:search_top, 0:w] = 0
				mask[search_bot:h, 0:w] = 0
				M = cv2.moments(mask)
				if M['m00'] > 0:
					cx = int(M['m10']/M['m00'])
					cy = int(M['m01']/M['m00'])
					err = cx - w/2
					self.twist.linear.x = 0.1
					self.twist.angular.z = -float(err) / 100
					self.cmd_vel_pub.publish(self.twist)
					cv2.imshow("mask",mask)
					cv2.imshow("output", image)
					cv2.waitKey(3)
			if (C3.data<1 or E5.data<1 or A4.data<1):
					self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
					self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
					self.cmd_vel_pub.publish(self.twist)
					print('agora foi?')

					if (E3.data<1.0):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
						self.cmd_vel_pub.publish(self.twist)
						print('1')

					elif (A3.data<1.0):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = ang
						self.cmd_vel_pub.publish(self.twist)


					#sensor da direita
					elif (E4.data<1.0):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = ang
						self.cmd_vel_pub.publish(self.twist)
						print("direita E")

					elif (C4.data<1.0):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = 0
						self.cmd_vel_pub.publish(self.twist)
						print("direita C")

					elif (B4.data<0.8):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = ang
						self.cmd_vel_pub.publish(self.twist)
						print("direita B")

					elif (A4.data<1):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
						self.cmd_vel_pub.publish(self.twist)
						print("direita A")
						image = self.bridge.imgmsg_to_cv2(msg,desired_encoding='bgr8')
						hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
						lower_yellow = numpy.array([ 10,  10,  10])
						upper_yellow = numpy.array([255, 255, 250])
						mask = cv2.inRange(hsv, lower_yellow, upper_yellow)

						h, w, d = image.shape
						search_top = 3*h/4
						search_bot = 3*h/4 + 20
						mask[0:search_top, 0:w] = 0
						mask[search_bot:h, 0:w] = 0
						M = cv2.moments(mask)
						if M['m00'] > 0:
							cx = int(M['m10']/M['m00'])
							cy = int(M['m01']/M['m00'])
							err = cx - w/2
							self.twist.linear.x = 0.1
							self.twist.angular.z = -float(err) / 100
							self.cmd_vel_pub.publish(self.twist)
							cv2.imshow("mask",mask)
							cv2.imshow("output", image)
							cv2.waitKey(3)

					#sensor da esquerda
					elif (A5.data<1.0):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
						self.cmd_vel_pub.publish(self.twist)
						print("A")

					elif (C5.data<1.0):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = 0
						self.cmd_vel_pub.publish(self.twist)
						print("C")

					elif (D5.data<0.8):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
						self.cmd_vel_pub.publish(self.twist)
						print("D")

					elif (E5.data<1):
						self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
						self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = ang
						self.cmd_vel_pub.publish(self.twist)
						print("E")
						image = self.bridge.imgmsg_to_cv2(msg,desired_encoding='bgr8')
						hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
						lower_yellow = numpy.array([ 10,  10,  10])
						upper_yellow = numpy.array([255, 255, 250])
						mask = cv2.inRange(hsv, lower_yellow, upper_yellow)

						h, w, d = image.shape
						search_top = 3*h/4
						search_bot = 3*h/4 + 20
						mask[0:search_top, 0:w] = 0
						mask[search_bot:h, 0:w] = 0
						M = cv2.moments(mask)
						if M['m00'] > 0:
							cx = int(M['m10']/M['m00'])
							cy = int(M['m01']/M['m00'])
							err = cx - w/2
							self.twist.linear.x = 0.1
							self.twist.angular.z = -float(err) / 100
							self.cmd_vel_pub.publish(self.twist)
							cv2.imshow("mask",mask)
							cv2.imshow("output", image)
							cv2.waitKey(3)

			else :
				if (E3.data<1 and E3.data<A3.data) or (D3.data>1) :
					self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
					self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
					self.cmd_vel_pub.publish(self.twist)
					print('E3 D3')

				elif (A3.data<1 and A3.data<E3.data) or (B3.data>1) :
					self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
					self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = ang
					self.cmd_vel_pub.publish(self.twist)
					print('A3 B3')

				elif (D3.data>B3.data):
					self.twist.linear.x = vel; self.twist.linear.y = 0; self.twist.linear.z = 0
					self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = ang
					self.cmd_vel_pub.publish(self.twist)
					print('d3 b3')
				
				#else :
				#	self.twist.linear.x =vel; self.twist.linear.y = 0; self.twist.linear.z = 0
				#	self.twist.angular.x = 0; self.twist.angular.y = 0; self.twist.angular.z = -ang
				#	self.cmd_vel_pub.publish(self.twist)
				#	print('b3 d3')


rospy.init_node('follower')
follower = Follower()
rospy.spin()
