set(_CATKIN_CURRENT_PACKAGE "gazebo_contact_plugin")
set(gazebo_contact_plugin_VERSION "0.0.0")
set(gazebo_contact_plugin_MAINTAINER "fabiana <fabiana@todo.todo>")
set(gazebo_contact_plugin_PACKAGE_FORMAT "2")
set(gazebo_contact_plugin_BUILD_DEPENDS "gazebo_ros" "roscpp")
set(gazebo_contact_plugin_BUILD_EXPORT_DEPENDS "gazebo_ros" "roscpp")
set(gazebo_contact_plugin_BUILDTOOL_DEPENDS "catkin")
set(gazebo_contact_plugin_BUILDTOOL_EXPORT_DEPENDS )
set(gazebo_contact_plugin_EXEC_DEPENDS "gazebo_ros" "roscpp")
set(gazebo_contact_plugin_RUN_DEPENDS "gazebo_ros" "roscpp")
set(gazebo_contact_plugin_TEST_DEPENDS )
set(gazebo_contact_plugin_DOC_DEPENDS )
set(gazebo_contact_plugin_URL_WEBSITE "")
set(gazebo_contact_plugin_URL_BUGTRACKER "")
set(gazebo_contact_plugin_URL_REPOSITORY "")
set(gazebo_contact_plugin_DEPRECATED "")