# CMake generated Testfile for 
# Source directory: /home/general_ws/src
# Build directory: /home/general_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(control)
subdirs(description)
subdirs(gazebo_contact_plugin)
